<?php
if ( ! empty($flash_message))
{
    ?>
    <div class="row">
        <div class="col-xl-12 col-lg-12">
            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-dismissible fade in mb-2">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $flash_message; ?>
            </div>
        </div>
    </div>
    <?php
}
?>

<section class="card">
    <div class="card-header">
        <h2 class="card-title">Dashboard</h2>
        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
        <div class="heading-elements">
            <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                <!--                <li><a href="#" class="btn btn-outline-primary btn-block btn-min-width mr-1 mb-1" role="button"><i class="icon-plus"></i> Add Student</a></li>-->
            </ul>
        </div>
    </div>
    <div class="card-body collapse in">
        <div class="card-block">
            <div class="card-text">
                <h3 style="text-align: center">Welcome Admin Section!</h3>
            </div>
        </div>
    </div>
</section>