<!-- main menu-->
<div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
    <!-- main menu header-->
    <div class="main-menu-header">
        <h5 class="text-center"><?php echo ! empty($user_name) ? $user_name : 'Admin'; ?></h5>
    </div>
    <!-- / main menu header-->
    <!-- main menu content-->
    <div class="main-menu-content">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">

            <li class=" navigation-header"><span data-i18n="nav.category.support">Main</span><i data-toggle="tooltip" data-placement="right" data-original-title="Support"
                                                                                                class="icon-ellipsis icon-ellipsis"></i></li>

            <li class="<?php echo ($this->uri->segment(2) == 'dashboard') ? 'active' : ''; ?> nav-item">
                <a href="<?php echo base_url('admin/dashboard'); ?>"><i class="icon-home3"></i>
                    <span data-i18n="nav.form_layouts.form_layout_basic" class="menu-title">Dashboard</span>
                </a>
            </li>
            <li class=" nav-item"><a href="#"><i class="icon-android-funnel"></i><span data-i18n="nav.menu_levels.main" class="menu-title">Menu levels</span></a>
                <ul class="menu-content">
                    <li><a href="#" data-i18n="nav.menu_levels.second_level" class="menu-item">Second level</a>
                    </li>
                    <li><a href="#" data-i18n="nav.menu_levels.second_level_child.main" class="menu-item">Second level child</a>
                        <ul class="menu-content">
                            <li><a href="#" data-i18n="nav.menu_levels.second_level_child.third_level" class="menu-item">Third level</a>
                            </li>
                            <li><a href="#" data-i18n="nav.menu_levels.second_level_child.third_level_child.main" class="menu-item">Third level child</a>
                                <ul class="menu-content">
                                    <li><a href="#" data-i18n="nav.menu_levels.second_level_child.third_level_child.fourth_level1" class="menu-item">Fourth level</a>
                                    </li>
                                    <li><a href="#" data-i18n="nav.menu_levels.second_level_child.third_level_child.fourth_level2" class="menu-item">Fourth level</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- /main menu content-->
    <!-- main menu footer-->
    <!-- include includes/menu-footer-->
    <!-- main menu footer-->
</div>
<!-- / main menu-->